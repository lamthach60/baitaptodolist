import { TaskController } from "../controller/TaskController.js";
import { CongViec } from "../model/TaskModel.js";
import { TaskService } from "./TaskService.js";
let taskList = [];
let renderTable = (list) => {
  let contentHTML = "";
  let contentUndone = "";
  for (let i = 0; i < list.length; i++) {
    let congViec = list[i];
    if (congViec.trangthai == false) {
      contentHTML += `
    <li>
        <p>${congViec.name}</p>
        <span class="buttons_Delete" onclick="xoaCongViec(${congViec.id})"><i class="fa fa-trash-alt"></i></span>
        <span href="javascript:;" onclick="statusCongViec(${congViec.trangthai})"><i class="fa fa-trash-alt"></i></span>
    </li>`;
    } else {
      contentUndone += `
      <li>
          <p>${congViec.name}</p>
          <span class="buttons_Delete" onclick="xoaCongViec(${congViec.id})"><i class="fa fa-trash-alt"></i></span>
          <span href="javascript:;" onclick="statusCongViec(${congViec.trangthai})"><i class="fa fa-trash-alt"></i></span>
      </li>`;
    }
    document.getElementById("todo").innerHTML = contentUndone;
    document.getElementById("completed").innerHTML = contentHTML;
  }
};
let renderDanhSachTask = () => {
  TaskService.layDanhSachTask()
    .then((res) => {
      console.log(res);
      taskList = res.data.map((congViec) => {
        let { id, name, mota, trangthai } = congViec;
        return new CongViec(id, name, mota, trangthai);
      });
      renderTable(taskList);
    })
    .catch((err) => {
      console.log(err);
    });
};
renderDanhSachTask();

let themCongViec = () => {
  let congViec = TaskController.layThongTinTuForm();
  TaskService.themMoiMonAn(congViec)
    .then((res) => {
      console.log(res);

      console.log("i");
    })
    .catch((err) => {
      console.log(err);
    });
};
window.themCongViec = themCongViec;

let xoaCongViec = (id) => {
  TaskService.xoaCongViec(id)
    .then((res) => {
      console.log(res);
      renderDanhSachTask();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.xoaCongViec = xoaCongViec;

let statusCongViec = (trangthai) => {
  if (trangthai == true) {
    console.log("true");
    renderDanhSachTask();
  } else {
    console.log("fales");
  }
};
window.statusCongViec = statusCongViec;
