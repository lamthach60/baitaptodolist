const BASE_URL = "https://62b07880196a9e98702448ba.mockapi.io/Mon-an";
export let TaskService = {
  layDanhSachTask: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  themMoiMonAn: (congViec) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: congViec,
    });
  },
  xoaCongViec: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },
};
