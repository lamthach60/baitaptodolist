export class CongViec {
  constructor(_id, _name, _mota, _trangthai) {
    this.id = _id;
    this.name = _name;
    this.mota = _mota;
    this.trangthai = _trangthai;
  }
}
